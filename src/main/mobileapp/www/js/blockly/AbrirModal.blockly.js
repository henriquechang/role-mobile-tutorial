window.blockly = window.blockly || {};
window.blockly.js = window.blockly.js || {};
window.blockly.js.blockly = window.blockly.js.blockly || {};
window.blockly.js.blockly.AbrirModal = window.blockly.js.blockly.AbrirModal || {};

/**
 * AbrirModal
 */
window.blockly.js.blockly.AbrirModal.Executar = function() {
 var item;
  this.cronapi.util.executeJavascriptNoReturn('RoleGrid.startInserting()');
  this.cronapi.screen.showIonicModal("modal54755");
}
